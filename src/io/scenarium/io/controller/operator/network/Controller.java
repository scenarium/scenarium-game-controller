/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.io.controller.operator.network;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import io.beanmanager.editors.DynamicChoiceBox;
import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.DynamicPossibilities;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.UpdatableViewBean;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.struct.Selection;
import io.scenarium.core.tools.LibraryUtils;
import io.scenarium.flow.BlockInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.io.controller.internal.Log;
import net.java.games.input.Component;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

@BlockInfo(info = "Block to get information from a controller. It include stick, gamepad, wheel and fingerstick. Please note, this block doesn't handle controller hotplug")
public class Controller extends EvolvedOperator implements DynamicEnableBean, UpdatableViewBean {
	private static boolean isAvailable;
	@PropertyInfo(index = 0, info = "Selected controller as data source. The type of controller can be a stick, a gamepad, a wheel or a fingerstick.")
	@DynamicPossibilities(possibleChoicesMethod = "getAvailableControllers", useSwingWorker = false)
	private String controller;
	@PropertyInfo(index = 1, info = "Rest time between each controller event. Used to limit the amount of output data.", unit = "ms")
	@NumberInfo(min = 0)
	private int restTime = 0;
	@PropertyInfo(index = 2, nullable = false, info = "Selected component as output data")
	@DynamicChoiceBox(possibleChoicesMethod = "getComponentList")
	private Selection<String> components = new Selection<>(new HashSet<>(), String.class);

	private net.java.games.input.Controller controllerRef;
	private final HashMap<String, Integer> controllerMap = new HashMap<>();
	private Object[] outputValueBuff;
	private long[] outputTsBuff;
	private Thread controllerThread;

	static {
		Path jinputPath = Path.of("jinput-2.0.9-natives-all");
		String osName = System.getProperty("os.name", "").trim();
		String[] libraryNames = null;
		if (osName.equals("Linux"))
			libraryNames = new String[] { "jinput-linux64" };
		else if (osName.equals("Mac OS X"))
			libraryNames = new String[] { "jinput-osx" };
		else if (osName.startsWith("Windows"))
			libraryNames = new String[] { "jinput-dx8_64", "jinput-raw_64" };
		if (libraryNames == null) {
			Log.error("LOADING: " + Controller.class.getSimpleName() + ": No library for" + osName);
			isAvailable = false;
		} else {
			System.setProperty("net.java.games.input.librarypath", LibraryUtils.getLibraryPath(jinputPath));
			System.setProperty("jinput.loglevel", "OFF");
			isAvailable = LibraryUtils.extractLibrariesFromRessources(Controller.class, null, jinputPath, false, libraryNames);
		}
	}

	@Override
	public void updateIOStructure() {
		if (this.controllerRef == null)
			return;
		Component[] components = this.controllerRef.getComponents();
		HashSet<String> selectedComponents = this.components.getSelected();
		String[] names = new String[selectedComponents.size()];
		Class<?>[] types = new Class<?>[names.length];
		this.controllerMap.clear();
		int outputIndex = 0;
		for (Component component : components)
			if (selectedComponents.contains(component.getName())) {
				names[outputIndex] = component.getName();
				types[outputIndex] = component.isAnalog() || component.getIdentifier() == Component.Identifier.Axis.POV ? Float.class : Boolean.class;
				this.controllerMap.put(component.getName(), outputIndex++);
			}
		updateOutputs(names, types);
		this.outputValueBuff = new Object[names.length];
		this.outputTsBuff = new long[this.outputValueBuff.length];
	}

	@Override
	public void birth() {
		if (this.controllerRef == null)
			return;
		onStart(() -> startControllerThread());
		onPause(() -> death());
		onResume(() -> startControllerThread());
	}

	private void startControllerThread() {
		this.controllerThread = new Thread() {
			@Override
			public void run() {
				Controller.this.controllerRef.poll();
				for (Component comp : Controller.this.controllerRef.getComponents()) {
					float value = comp.getPollData();
					Integer outputIndex = Controller.this.controllerMap.get(comp.getName());
					if (outputIndex != null) {
						Controller.this.outputValueBuff[outputIndex] = comp.isAnalog() || comp.getIdentifier() == Component.Identifier.Axis.POV ? value : value == 1.0f;
						Controller.this.outputTsBuff[outputIndex] = System.currentTimeMillis();
					}
				}
				triggerOutput(Controller.this.outputValueBuff, Controller.this.outputTsBuff);
				Arrays.fill(Controller.this.outputValueBuff, null);
				while (!isInterrupted()) {
					Controller.this.controllerRef.poll();
					EventQueue queue = Controller.this.controllerRef.getEventQueue();
					Event event = new Event();
					while (queue.getNextEvent(event)) {
						Component comp = event.getComponent();
						Integer outputIndex = Controller.this.controllerMap.get(comp.getName());
						if (outputIndex != null) {
							float value = event.getValue();
							Controller.this.outputValueBuff[outputIndex] = comp.isAnalog() || comp.getIdentifier() == Component.Identifier.Axis.POV ? value : value == 1.0f;
							Controller.this.outputTsBuff[outputIndex] = System.currentTimeMillis();
							triggerOutput(Controller.this.outputValueBuff, Controller.this.outputTsBuff);
							Controller.this.outputValueBuff[outputIndex] = null;
						}
					}
					if (Controller.this.restTime != 0)
						try {
							Thread.sleep(Controller.this.restTime);
						} catch (InterruptedException e) {
							return;
						}
				}
			}
		};
		this.controllerThread.setName(getBlockName() + " controller thread");
		this.controllerThread.start();
	}

	public void process() {}

	@Override
	public void death() {
		if (this.controllerThread != null) {
			this.controllerThread.interrupt();
			try {
				this.controllerThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.controllerThread = null;
		}
	}

	public String getController() {
		return this.controller;
	}

	public void setController(String controller) {
		var oldValue = this.controller;
		this.controller = controller;
		runLater(() -> {
			if (isRunning())
				death();
			this.controllerRef = null;
			if (isAvailable)
				for (net.java.games.input.Controller c : ControllerEnvironment.getDefaultEnvironment().getControllers())
					if (c.getName().equals(this.controller)) {
						this.controllerRef = c;
						break;
					}
			updateComponentFilters();
			setEnable();
			updateView();
			if (isRunning())
				startControllerThread();
		});
		this.pcs.firePropertyChange("controller", oldValue, controller);
	}

	private void updateComponentFilters() {
		if (this.controllerRef == null)
			this.components = new Selection<>(new HashSet<>(), String.class);
		else {
			ArrayList<Object> toRemove = new ArrayList<>();
			List<String> components = Arrays.stream(this.controllerRef.getComponents()).map(c -> c.getName()).collect(Collectors.toList());
			for (String selection : this.components.getSelected())
				if (!components.contains(selection))
					toRemove.add(selection);
			if (!toRemove.isEmpty()) {
				HashSet<String> sel = this.components.getSelected();
				sel.removeAll(toRemove);
				this.components = new Selection<>(sel, String.class);
			}
		}
		updateIOStructure();
	}

	public int getRestTime() {
		return this.restTime;
	}

	public void setRestTime(int restTime) {
		var oldValue = this.restTime;
		this.restTime = restTime;
		this.pcs.firePropertyChange("restTime", oldValue, restTime);
	}

	public Selection<String> getComponents() {
		return this.components;
	}

	public void setComponents(Selection<String> components) {
		var oldValue = this.components;
		this.components = components;
		this.pcs.firePropertyChange("buttons", oldValue, components);
		updateIOStructure();
	}

	public String[] getComponentList() {
		if (this.controllerRef == null)
			return new String[0];
		return Arrays.stream(this.controllerRef.getComponents()).map(c -> c.getName()).toArray(String[]::new);
	}

	public String[] getAvailableControllers() {
		if (!isAvailable)
			return new String[0];
		ArrayList<String> availableControllers = new ArrayList<>();
		net.java.games.input.Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
		for (int i = 0; i < controllers.length; i++) {
			net.java.games.input.Controller controller = controllers[i];
			if (controller.getType() == net.java.games.input.Controller.Type.STICK || controller.getType() == net.java.games.input.Controller.Type.GAMEPAD
					|| controller.getType() == net.java.games.input.Controller.Type.WHEEL || controller.getType() == net.java.games.input.Controller.Type.FINGERSTICK)
				availableControllers.add(controller.getName());
		}
		return availableControllers.toArray(String[]::new);
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "components", this.controllerRef != null);
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "components", false);
	}
}
