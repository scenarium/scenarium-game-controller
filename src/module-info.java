import io.scenarium.io.controller.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.controller {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;
	requires jinput;

	exports io.scenarium.io.controller;
	exports io.scenarium.io.controller.operator.network;

}
